# SWT Charts Extensions

Fork of https://projects.eclipse.org/projects/science.swtchart/

## Usage

Gradle
```groovy
repositories {
    maven {  url "https://michaelsp.github.io/maven-repo" }
}

dependency {
    implementation "com.github.michaelsp:org.eclipse.swtchart.extensions:0.8.0"
}
```

Maven
```xml
<repositories>
    <repository>
        <id>michaelsp</id>
        <url>https://michaelsp.github.io/maven-repo</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
      <groupId>com.github.michaelsp</groupId>
      <artifactId>org.eclipse.swtchart.extensions</artifactId>
      <version>0.8.0</version>
    </dependency>
</dependencies>
```